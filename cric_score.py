#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Apr 16 23:55:13 2017

@author: piyush
"""

try:
    import lxml.etree as etree
except ImportError:
    import xml.etree.ElementTree as etree

COLORED = False

try:
    from termcolor import colored
    COLORED = True
except ImportError:
    pass



URL = 'http://static.cricinfo.com/rss/livescores.xml'

tree = etree.parse(URL)
root = tree.getroot()

items = root.findall('.//item')

for item in items:
    text = item.find('title').text
    if COLORED:
        text = colored(text, 'red', attrs=['reverse', 'blink'])
    print(text)
    print('------------')



